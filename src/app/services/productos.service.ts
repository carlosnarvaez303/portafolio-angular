import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Producto } from '../interfaces/info-productos.interface';



@Injectable({
  providedIn: 'root'
})
export class ProductosService {

    cargando = true;
    productos: Producto[] = [];
    productosFiltrado: Producto[] = [];

  constructor( private Http: HttpClient) {

    this.cargarProductos();
  }

   // tslint:disable-next-line: typedef
   private cargarProductos() {

    // tslint:disable-next-line: no-shadowed-variable
    return new Promise ( ( resolve, reject  ) => {

      this.Http.get('https://angular-html-ca5f0.firebaseio.com/productos_idx.json')
     .subscribe( (resp: Producto[]) => {
      this.productos = resp;

      setTimeout(() => {
        this.cargando = false;
      }, 2000);

      resolve();


    });

  });
  }
    // tslint:disable-next-line: typedef
    getProducto(id: string) {

     return this.Http.get(`https://angular-html-ca5f0.firebaseio.com/productos/${ id }.json`);

    }

    // tslint:disable-next-line: typedef
    buscarProducto(termino: string) {

      if (this.buscarProducto.length === 0){
        // cargar Productos
        this.cargarProductos().then( () => {
          // ejecutar despues de tener los productos
          // Aplicar el filtro
          this.filtrarProductos( termino );
        });
      } else {
        // Aplicar el Filtro
        this.filtrarProductos( termino );
      }
    }

    // tslint:disable-next-line: typedef
    private filtrarProductos( termino: string) {

      // console.log(this.productos);
      this.productosFiltrado = [];

      termino = termino.toLocaleLowerCase();

      this.productos.forEach( prod => {

        const tituloLower = prod.titulo.toLocaleLowerCase();

        if (prod.categoria.indexOf( termino ) >= 0 || tituloLower.indexOf( termino) >= 0 ) {
          this.productosFiltrado.push( prod );
        }

        });
    }
  }
